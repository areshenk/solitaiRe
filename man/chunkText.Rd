\name{chunkText}
\alias{chunkText}
\title{Create and pad text blocks of a specified size}
\usage{
    chunkText(x, chunk.length = 5, pad = T)
}
\arguments{
    \item{x}{A character string}
    \item{chunk.length}{Length of each text chunk}
    \item{pad}{If T, text will be padded with Xs until a multiple of chunk.length}
}
\value{
    A character string with a space every chunk.length letters.
}

\examples{
    # Strip text of non-alphabetic character
    x <- 'This is a test!'
    x.stripped <- stripText(x)
    
    # Create chunks of length 5
    chunkText(x.stripped, chunk.length = 5, pad = T)
}
