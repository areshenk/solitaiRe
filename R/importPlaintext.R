stripText <- function(x = NULL, file = NULL){

    # Check argument
    if (!xor(is.null(x), is.null(file))){
        stop('Must supply exactly one of "x" or a "file"')
    } else if (is.null(x)){
        raw <- readChar(file, file.info(file)$size)
    } else {
        raw <- x
    }

    # Strip all except alphanumeric
    clean <- tolower(str_replace_all(raw, "[^[:alnum:]]", ""))

    # Convert to charatcer vector
    return(clean)
}

chunkText <- function(x, chunk.length = 5, pad = T){
    
    # Pad with X to be a multiple of chunk.length
    if (pad){
        n <- nchar(x) %% chunk.length
        if (n > 0)
            x <- paste(x, paste(rep('x', chunk.length - n), 
                                    sep = '', collapse = ''),
                       sep = '')
    }
    
    gsub(paste('(.{', chunk.length, '})', sep = ''), "\\1 ", x)
}